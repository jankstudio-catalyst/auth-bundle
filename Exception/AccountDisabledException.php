<?php

namespace Catalyst\AuthBundle\Exception;

use Symfony\Component\Security\Core\Exception\AccountStatusException;

class AccountDisabledException extends AccountStatusException
{
    public function getMessageKey()
    {
        return 'Account has been disabled.';
    }
}
