<?php

namespace Catalyst\AuthBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

abstract class Role
{
    const SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=80)
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=80)
     */
    protected $name;

    // NOTE: annotation should be in the child class
    protected $users;

    // array of permissions this role has access to
    /**
     * @ORM\Column(type="json_array")
     */
    protected $acl_attributes;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->acl_attributes = [];
    }

    public function setID($id)
    {
        // example ROLE_SUPER_ADMIN, ROLE_CASHIER, etc
        $this->id = $id;
        return $this;
    }

    public function getID()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getUsers()
    {
        return $this->users;
    }

    public function getUsersCount()
    {
        return $this->users->count();
    }

    public function isSuperAdmin()
    {
        if ($this->id == self::SUPER_ADMIN)
            return true;

        return false;
    }

    public function clearACLAccess()
    {
        $this->acl_attributes = [];
        return $this;
    }

    public function getACLAttributes()
    {
        return $this->acl_attributes;
    }

    public function addACLAccess($attribute)
    {
        $this->acl_attributes[$attribute] = true;
        return $this;
    }

    public function hasACLAccess($attribute)
    {
        // if it's super admin, they always have access
        if ($this->isSuperAdmin())
            return true;

        // check ACL attributes
        if (isset($this->acl_attributes[$attribute]) && $this->acl_attributes[$attribute])
            return true;

        return false;
    }
}
