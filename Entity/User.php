<?php

namespace Catalyst\AuthBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Serializable;
use Symfony\Component\Security\Core\User\UserInterface;

// base User class
abstract class User implements UserInterface
{
	// NOTE: doctrine annotations for roles have to be declared on the child class
    protected $roles;

	/**
	 * @ORM\Column(type="boolean")
	 */
    protected $enabled;

    public function __construct()
    {
        $this->roles = new ArrayCollection();
        $this->enabled = true;
    }

    // array of string roles, needed by symfony
    public function getRoles()
    {
        $str_roles = [];
        foreach ($this->roles as $role)
            $str_roles[] = $role->getID();

        return $str_roles;
    }

    public function getRoleObjects()
    {
        return $this->roles;
    }

    public function addRole(Role $role)
    {
        $this->roles[$role->getID()] = $role;
        return $this;
    }

    public function clearRoles()
    {
        $this->roles->clear();
        return $this;
    }

    public function setEnabled($enabled = true)
    {
        $this->enabled = $enabled;
        return $this;
    }

    public function isEnabled()
    {
        return $this->enabled;
    }

    public function hasRole($role_id)
    {
        if (isset($this->roles[$role_id]))
            return true;

        return false;
    }

    public function isSuperAdmin()
    {
        return $this->hasRole(Role::SUPER_ADMIN);
    }

    public function setSalt($salt)
    {
        // do nothing for now
        // NOTE: if you want to use salt, extend and add
        return $this;
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
        return $this;
    }

    public function serialize()
    {
        return serialize([
            $this->id,
            $this->username,
            $this->password,
            $this->enabled,
        ]);
    }

    public function unserialize($serial)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            $this->enabled,
        ) = unserialize($serial);
    }
}
