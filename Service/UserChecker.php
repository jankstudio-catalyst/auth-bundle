<?php

namespace Catalyst\AuthBundle\Service;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Catalyst\AuthBundle\Entity\User;
use Catalyst\AuthBundle\Exception\AccountDisabledException;

class UserChecker implements UserCheckerInterface
{
    public function checkPreAuth(UserInterface $user)
    {
        // do nothing
        return;
    }

    public function checkPostAuth(UserInterface $user)
    {
        // handle catalyst suth users
        if (!($user instanceof User))
            return;
        
        // check if enabled
        if (!$user->isEnabled())
        {
            throw new AccountDisabledException("Account has been disabled.");
        }
    }
}
