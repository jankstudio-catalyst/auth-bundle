<?php

namespace Catalyst\AuthBundle\Service;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter as BaseVoter;
use Symfony\Component\Security\Core\Security;

class ACLVoter extends BaseVoter
{
    protected $acl_gen;
    protected $user_class;
    protected $security;

    public function __construct(Security $security, ACLGenerator $acl_gen, $user_class)
    {
        $this->acl_gen = $acl_gen;
        $this->user_class = $user_class;
        $this->security = $security;
    }

    protected function supports($attribute, $subject)
    {
        // NOTE: we currently do not check for subject, we'll leave that to other voters

        // check if it's using our user class
        $user = $this->security->getUser();
        if (!($user instanceof $this->user_class))
            return false;

        // check if the attribute is in our acl key index
        $acl_data = $this->acl_gen->getACL();
        if (isset($acl_data['index'][$attribute]))
            return true;

        return false;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        // check if any of the user's roles have access
        $roles = $user->getRoleObjects();
        foreach ($roles as $role)
        {
            if ($role->hasACLAccess($attribute))
                return true;
        }

        return false;
    }
}

