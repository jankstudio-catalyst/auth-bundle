<?php

namespace Catalyst\AuthBundle\Service;

use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Symfony\Component\Yaml\Parser as YamlParser;
use Symfony\Component\Config\ConfigCache;
use Symfony\Component\Config\Resource\FileResource;

use Symfony\Component\Routing\RouterInterface;

class ACLGenerator
{
    const ACCESS_KEY        = 'access_keys';

    protected $router;
    protected $cache_dir;
    protected $config_dir;
    protected $acl_file;

    public function __construct(RouterInterface $router, $cache_dir, $config_dir, $acl_file)
    {
        $this->router = $router;
        $this->cache_dir = $cache_dir;
        $this->config_dir = $config_dir;
        $this->acl_file = $acl_file;
    }

    public function getACL()
    {
        // cache config
        $cache_file = $this->cache_dir . '/' . $this->acl_file . '.serial';
        $cache = new ConfigCache($cache_file, true);

        // check if cache is fresh
        if (!$cache->isFresh())
        {
            $files = [];
            $resources = [];

            try
            {
                // get location of the yaml file with the acls 
                // $path = $this->config_dir . '/<acl yaml filename>';
                $path = $this->config_dir . '/' . $this->acl_file;

                $files[] = $path;
                $resources[] = new FileResource($path);

                // process acl config file
                $data = $this->parseACL($path);
            }
            catch (\InvalidArgumentException $e)
            {
                error_log($e->getMessage());
                error_log(self::ACCESS_KEY . ' key not found in ' . $this->acl_file . 'file.');
                return $data;
            }

            $acl_serial = serialize($data);
            $cache->write($acl_serial, $resources);
        }
        else
        {
            $acl_serial = file_get_contents($cache_file);
            $data = unserialize($acl_serial);
        }

        return $data;
    }

    protected function parseACL($path)
    {

        $parser = new YamlParser();
        $config = $parser->parse(file_get_contents($path));

        // check if we have access keys 
        if (!isset($config[self::ACCESS_KEY]))
        {
            error_log('No ' . self::ACCESS_KEY . ' found for ' . $path);
            return;
        }

        $acl_hierarchy = [];
        $acl_index = [];

        // go through each one
        foreach ($config[self::ACCESS_KEY] as $acl_data)
        {
            // build hierarchy
            $acl_hierarchy[$acl_data['id']] = [
                'label' => $acl_data['label'],
                'acls' => []
            ];

            foreach ($acl_data['acls'] as $acl)
            {
                $id = $acl['id'];
                $label = $acl['label'];

                // set hierarchy and index
                $acl_hierarchy[$acl_data['id']]['acls'][$id] = $label;
                $acl_index[$id] = $label;
            }
        }

        return [
            'hierarchy' => $acl_hierarchy,
            'index' => $acl_index
        ];
    }
}
