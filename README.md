# Catalyst Auth Bundle


The Catalyst Auth Bundle was built to provide role-based access control (RBAC) support for Symfony applications. It contains abstract user and role classes as well as access decision voter and user checker services.

## Installation

* Add repository to composer.json
```json
"repositories": [
    { "type": "vcs", "url": "https://gitlab.com/jankstudio-catalyst/catalyst-auth.git" }
]
```

* Add catalyst/auth-bundle to composer.json by running:
```sh
composer require catalyst/auth-bundle dev-master
```

## Configuration

* Add the bundle services to the services.yaml file
```yaml
services:
    Catalyst\AuthBundle\Service\ACLGenerator:
        arguments:
            $router: "@router.default"
            $cache_dir: "%kernel.cache_dir%"
            $config_dir: "%kernel.root_dir%/../config"
            $acl_file: "acl_file.yaml"

    Catalyst\AuthBundle\Service\ACLVoter:
        arguments:
            $user_class: "App\\Entity\\User"
        tags: ['security.voter']

    Catalyst\AuthBundle\Service\UserChecker:
```

   acl_file parameter for ACLGenerator is the configuration file for the ACL keys.  
   config_dir parameter for ACLGenerator is the config directory where the yaml file will be located.  
   user_class parameter for ACLVoter is the user entity to be used.  

* Add the ACL yaml file:
```yaml
access_keys:
    - id: section
      label: Section Label
      acls:
        - id: section.menu
          label: Menu
        - id: section.list
          label: List
        - id: section.add
          label: Add New
        - id: section.edit
          label: Edit
        - id: section.delete
          label: Delete
    - id: another_section
      label: Another Section
      acls:
        - id: asec.menu
          label: Menu
        - id: asec.list
          label: List
        - id: asec.something
          label: Something
```

* Create user entity that extends Catalyst\AuthBundle\Entity\User

* Create role entity that extends Catalyst\AuthBundle\Entity\Role

* Make sure there's a role with id = "ROLE_SUPER_ADMIN"

* Make sure user_checker and access_decision_manager are set in firewall in config/packages/security.yaml:
```yaml
security:
    firewalls:
        main:
            user_checker: Catalyst\AuthBundle\Service\UserChecker
    access_decision_manager:
        strategy: unanimous
```

## Usage

You can use the denyAccessUnlessGranted method within the controller:
```php
$this->denyAccessUnlessGranted('section.list', null, 'No access.');
```

You can also use the is_granted function in twig templates:
```twig
{% if is_granted('section.list') %}
...
{% endif %}
```
